from abc import ABC, abstractmethod
import random
import sys

class Sensor(ABC):
    __slots__ = ['sensor_id', 'location']

    def __init__(self, sensor_id, location,*args):
        self.sensor_id = sensor_id
        self.location = location
    
    @classmethod
    def read_data(self):
        ...
        
class TemperatureSensor(Sensor):
    __slots__ = ('temperature') 

    def __init__(self, sensor_id, location, temperature):
        super().__init__(sensor_id, location)
        self.temperature = temperature
    
    def read_data(self):
        print(f"Temperature sensor current state-> (ID: {self.sensor_id}, Location: {self.location}): {self.temperature}°C")
        self.temperature+= round(10 + (5 * random.random()), 2)  
        print(f"Temperature sensor state after changes-> (ID: {self.sensor_id}, Location: {self.location}): {self.temperature}°C")
             
class HumiditySensor(Sensor):
    __slots__ = ('humidity') 

    def __init__(self, sensor_id, location,humidity):
        super().__init__(sensor_id, location)
        self.humidity = humidity
    
    def read_data(self):
        print(f"Humidity sensor current state-> (ID: {self.sensor_id}, Location: {self.location}): {self.humidity}%")
        self.humidity+= round(10+ (10 * random.random()), 2)  
        print(f"Humidity sensor state after changes-> (ID: {self.sensor_id}, Location: {self.location}): {self.humidity}%")
            
         
class LightSensor(Sensor):
    __slots__ = ('light_intensity') 

    def __init__(self,sensor_id, location,light_intensity):
        self.light_intensity = light_intensity
        super().__init__(sensor_id, location)
        
    def read_data(self):
        print(f"Light sensor current state-> (ID: {self.sensor_id}, Location: {self.location}): {self.light_intensity} lux") 
        self.light_intensity += round(40 * random.random())  
        print(f"Light sensor state after changes-> (ID: {self.sensor_id}, Location: {self.location}): {self.light_intensity} lux")    
            
class SensorManager:
    def __init__(self):
        self.sensors = []

    def add_sensor(self, sensor):
        if isinstance(sensor, Sensor):
          self.sensors.append(sensor)
        else:
          print("Error: Invalid sensor type. Please add a subclass of Sensor.")

    def remove_sensor(self, sensor_id):
        try:
            self.sensors.remove(sensor for sensor in self.sensors if sensor.sensor_id == sensor_id)
        except ValueError:
            print(f"Error: Sensor with ID {sensor_id} not found.")

    def read_all_data(self):
        for sensor in self.sensors:
          sensor.read_data()


class Sensor2(Sensor):
  __slots__ = ['sensor_id', 'location']

  def __init__(self, sensor_id, location,*args):
    super().__init__(sensor_id, location)  

  def read_data(self):
    print("Data reading from Sensor2")
    
temp_sens=TemperatureSensor(1,"Greenhouse",10)
hyum_sens=HumiditySensor(2,"Greenhouse",20)
light_sens=LightSensor(3, "Greenhouse",40)

sens_manager=SensorManager()
sens_manager.add_sensor(temp_sens)
sens_manager.add_sensor(hyum_sens)
sens_manager.add_sensor(light_sens)

sens_manager.read_all_data()

sensor_with_slots = Sensor(1, "Greenhouse",10)
sensor_without_slots = Sensor2(1, "Greenhouse",10)

print(f"Memory usage of with slots: {sys.getsizeof(sensor_with_slots)} bytes")
print(f"Memory usage of without slots: {sys.getsizeof(sensor_without_slots)} bytes")

                           
         